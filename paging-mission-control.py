# Python 3.9
# Requires pip, pandas

import sys, json
import pandas as pd
from datetime import datetime


# Specify input file name as first argument when calling python
# Usage: python paging-mission-control.py <filename>.txt
# If no arguments specified, default to hardcoded value

try:
    input = sys.argv[1]

except:
    input = 'testinput.txt'

# Create initial dataframe
df = pd.read_csv(input, sep='|', header=None, names=['timestamp', 'satellite-id', 'red-high-limit',
                                                        'yellow-high-limit', 'yellow-low-limit', 'red-low-limit', 
                                                        'raw-value', 'component'])

df.sort_values(by=['timestamp'], inplace=True)


# Get list of unique satellites
sats = df['satellite-id'].unique()

# Create empty list to hold alerts
alerts = []

# Create function to generate alerts
# generate_alerts will accept a single dataframe, containing rows for a single
# satellite and component which violate the limits.  It will check to see if
# three violations occur in a five minute period, and, if so, return formatted alerts.

def generate_alerts(df, severity):

    # Create list to hold readings which are already covered in an alert
    # Assumes that if an alert has been generated, all 3 readings above/below limit should 
    # be disqualified from generating future alerts
    alerted = []

    # For each row, check to see if next two entries are within 5 minutes
    for i in range(df.shape[0]):

        if df.iloc[i,0] in alerted:
            continue
        
        try:
            d1 = datetime.strptime(df.iloc[i,0], '%Y%m%d %H:%M:%S.%f')
            d2 = datetime.strptime(df.iloc[i+2,0], '%Y%m%d %H:%M:%S.%f')

            # If 3 entries within 5 minute period, add alert
            # Note that alert timestamp will be for the first limit violation
            # To use timestamp of last limit violation, replace d1 below with d2.
            if (d2 - d1).total_seconds() < 300:
                alerts.append({'satelliteId' : int(df.iloc[i,1]), 'severity' : severity, 'component': df.iloc[i,7], 
                               'timestamp' : d1.strftime('%Y-%M-%DT%H:%M:%S.%fZ')})

                alerted.append(df.iloc[i,0])
                alerted.append(df.iloc[i+1,0])
                alerted.append(df.iloc[i+2,0])

        except IndexError:
            pass

        except Exception as e:
            print(e)

for sat in sats:

    # Check for battery levels under red-low-limit

    df_temp = df.loc[(df['component']=='BATT') & (df['raw-value']<df['red-low-limit']) & (df['satellite-id']==sat)]
    generate_alerts(df_temp, "RED LOW")


    # Check for temperature above red-high-limit

    df_temp = df.loc[(df['component']=='TSTAT') & (df['raw-value']>df['red-high-limit']) & (df['satellite-id']==sat)]
    generate_alerts(df_temp, 'RED HIGH')

print(json.dumps(alerts, indent=4))